package com.classpath.day3reactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Day3ReactiveApplication {

    public static void main(String[] args) {
        SpringApplication.run(Day3ReactiveApplication.class, args);
    }

}
