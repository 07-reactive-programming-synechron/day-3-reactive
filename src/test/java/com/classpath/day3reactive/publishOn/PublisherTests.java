package com.classpath.day3reactive.publishOn;

import org.junit.jupiter.api.Test;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.List;

public class PublisherTests {

    @Test
    public void testPublishOn() throws InterruptedException {
        Flux<Integer> rating = Flux
                                .fromIterable(List.of("apple", "samsung", "lg", "oppo"))
                                .publishOn(Schedulers.parallel())
                                    .map(brand -> {
                                        System.out.println("Processing of "+ brand+ " is done in the "+ Thread.currentThread().getName());
                                        return brand.length();
                                    });

        //task
        Thread t1 = new Thread(() -> rating.subscribe(value -> System.out.println(" Processing the value " + value + " with " + Thread.currentThread().getName())), "thread - 1 " );
       // Thread t2 = new Thread(() -> rating.subscribe(value -> System.out.println(" Processing the value " + value + " with " + Thread.currentThread().getName())), "thread - 2 " );

        t1.start();
        //t2.start();

        t1.join();
        //t2.join();
    }

    @Test
    public void testSubscribe() throws InterruptedException {
        Flux<Integer> rating = Flux
                .fromIterable(List.of("apple", "samsung", "lg", "oppo"))
                .map(brand -> {
                    System.out.println("Processing of "+ brand+ " is done in the "+ Thread.currentThread().getName());
                    return brand.length();
                })
                .delayElements(Duration.ofSeconds(2));
        //task
        Thread t1 = new Thread(() -> rating.subscribeOn(Schedulers.newParallel("scheduler-a", 2)).subscribe(value -> System.out.println(" Processing the value " + value + " with " + Thread.currentThread().getName())));
        Thread boundedElastic = new Thread(() -> rating.subscribeOn(Schedulers.boundedElastic()).subscribe(value -> System.out.println(" Processing the value " + value + " with " + Thread.currentThread().getName())) );
        //Thread t2 = new Thread(() -> rating.subscribeOn(Schedulers.boundedElastic()).subscribe(value -> System.out.println(" Processing the value " + value + " with " + Thread.currentThread().getName())), "thread - 2 " );

        t1.start();
        //t2.start();

        t1.join();
        //t2.join();

        Thread.sleep(20000);

    }
}