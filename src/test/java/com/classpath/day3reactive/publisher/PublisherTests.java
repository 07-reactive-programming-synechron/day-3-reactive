package com.classpath.day3reactive.publisher;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.stream.Stream;

public class PublisherTests {

    //cold publisher
    @Test
    public void testColdPublisher() throws InterruptedException {
        Flux<String> events = Flux.fromStream(() -> fetchEvents())
                                    .doOnSubscribe(data -> System.out.println(" Subscription begin" + data))
                                    .doFinally(data -> System.out.println("Subscription completed "+ data))
                                    .delayElements(Duration.ofSeconds(2));

        events.subscribe(event -> System.out.println("Processing the event by subscriber  - 1 "+event));

        Thread.sleep(2000);
        events.subscribe(event -> System.out.println("Processing the event by subscriber  - 2 "+event));

        Thread.sleep( 20000);
    }
    //hold publisher
    @Test
    public void testHotPublisher() throws InterruptedException {
        Flux<String> events = Flux.fromStream(() -> fetchEvents()).delayElements(Duration.ofSeconds(1)).share();

        events.subscribe(event -> System.out.println("Processing the event by subscriber  - 1 "+event));

        Thread.sleep(10000);
        events.subscribe(event -> System.out.println("Processing the event by subscriber  - 2 "+event));

        Thread.sleep( 20000);
    }


    private Stream<String> fetchEvents(){
        return Stream.of(
                "step-1",
                "step-2",
                "step-3",
                "step-4",
                "step-5",
                "step-6",
                "step-7",
                "step-8"
        );
    }
}


