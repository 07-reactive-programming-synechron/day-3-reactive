package com.classpath.day3reactive.retry;

import org.junit.jupiter.api.Test;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import reactor.util.retry.RetryBackoffSpec;

import java.time.Duration;

public class RetryTests {

    @Test
    public void testWithRetry () {
        Flux<String> agesForCastingVote = fetchUsersAge()
                .map(RetryTests::validateAgeForCastingVote)
                .doOnError(error -> System.out.println("Error :: " + error))
                .retry(3);

        agesForCastingVote

                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onSubscribe(Subscription s) {
                        s.request(15);
                    }

                    @Override
                    public void onNext(String s) {
                        System.out.println(" Processing the value "+ s);
                    }

                    @Override
                    public void onError(Throwable t) {
                        System.out.println(" Error :: "+ t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Test
    public void testMonoWithRetry (){
        Mono.just(4)
                .map(value -> {
                    if (value % 2 == 0){
                        throw new IllegalArgumentException(" Invalid data")
                    }
                    return value;
                })
               .doOnError(error -> System.out.println("Error " +error))
               .retryWhen(Retry.backoff(2, Duration.ofSeconds(2)));

        Mono.just(8)
                .map(value -> {
                    if (value % 2 == 0){
                        throw new IllegalArgumentException(" Invalid data")
                    }
                    return value;
                })
               .doOnError(error -> System.out.println("Error " +error))
        //       .retryWhen(Retry.fixedDelay(2, Duration.ofSeconds(2)));
               .retryWhen(RetryBackoffSpec.backoff(3, Duration.ofSeconds(2)));

    }

    private Flux<Integer> fetchUsersAge() {
        return Flux.just(22,33,45,18, 56,17);
    }

    private static String validateAgeForCastingVote (Integer value){
        if (value < 18) {
            throw new IllegalArgumentException("Invalid age for casting vote");
        }
        return "Number :: "+ value;
    }

    @Test
    public void testSwitchIfEmpty(){
        emptyFlux().switchIfEmpty(Flux.error(new RuntimeException(" invalid value")));
    }

    private Flux<String> emptyFlux(){
        return Flux.empty();
    }

}